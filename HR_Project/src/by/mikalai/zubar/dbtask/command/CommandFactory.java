package by.mikalai.zubar.dbtask.command;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import by.mikalai.zubar.dbtask.command.admin.ActivateCommand;
import by.mikalai.zubar.dbtask.command.admin.AddRecruiterCommand;
import by.mikalai.zubar.dbtask.command.admin.DeleteRecruiterCommand;
import by.mikalai.zubar.dbtask.command.admin.DisplayRecruiterCommand;
import by.mikalai.zubar.dbtask.command.admin.DisplayVacancyAdminCommand;
import by.mikalai.zubar.dbtask.command.admin.RedirectVacToNewRecCommand;
import by.mikalai.zubar.dbtask.command.candidate.AddCandidateCommand;
import by.mikalai.zubar.dbtask.command.candidate.AssignVacancyCommand;
import by.mikalai.zubar.dbtask.command.candidate.DisclaimVacancyCommand;
import by.mikalai.zubar.dbtask.command.candidate.DisplayInterviewCommand;
import by.mikalai.zubar.dbtask.command.candidate.DisplayVacancyCommand;
import by.mikalai.zubar.dbtask.command.candidate.ReAssingVacancy;
import by.mikalai.zubar.dbtask.command.recruiter.AddNewVacancy;
import by.mikalai.zubar.dbtask.command.recruiter.AssignInterviewForCandCommand;
import by.mikalai.zubar.dbtask.command.recruiter.AssignNewInterviewCommand;
import by.mikalai.zubar.dbtask.command.recruiter.CloseVacancyCommand;
import by.mikalai.zubar.dbtask.command.recruiter.DisplayVacancyForRecCommand;
import by.mikalai.zubar.dbtask.command.recruiter.ShowInterviewCommand;
import by.mikalai.zubar.dbtask.command.recruiter.UpdateInterviewCommand;

/**
 * Returns a proper ICommand class depending on the 
 * value of "action" request parameter. The factory is constructed using
 * Singleton design pattern (allows to create only 1 instance of factory).
 * @author Mikalay Zubar
 *
 */
public class CommandFactory {
	
	private Map<String, ICommand> commandMap = new HashMap<>();
	
	private CommandFactory(){
		commandMap.put("changelang", new ChangeLanguageCommand());
		commandMap.put("addCand", new AddCandidateCommand());
		commandMap.put("authorization", new AuthorizationCommand());
		commandMap.put("showVacancy", new DisplayVacancyCommand());
		commandMap.put("assignVac", new AssignVacancyCommand());
		commandMap.put("disclaimVac", new DisclaimVacancyCommand());
		commandMap.put("reAssignVac", new ReAssingVacancy());
		commandMap.put("showInterview", new DisplayInterviewCommand());
		commandMap.put("showRecVacancy", new DisplayVacancyForRecCommand());
		commandMap.put("addNewVacancy", new AddNewVacancy());
		commandMap.put("assignAnInterview", new AssignNewInterviewCommand());
		commandMap.put("addNewInterview", new AssignInterviewForCandCommand());
		commandMap.put("showRecInterview", new ShowInterviewCommand());
		commandMap.put("closeVacancy", new CloseVacancyCommand());
		commandMap.put("updateInterview", new UpdateInterviewCommand());
		commandMap.put("addRecruiter", new AddRecruiterCommand());
		commandMap.put("showRecruiter", new DisplayRecruiterCommand());
		commandMap.put("showVacancyAdmin", new DisplayVacancyAdminCommand());
		commandMap.put("redirectVacancy", new RedirectVacToNewRecCommand());
		commandMap.put("goFirstPage", new GoFirstPageCommand());
		commandMap.put("logOut", new LogOutCommand());
		commandMap.put("deleteRecruiter", new DeleteRecruiterCommand());
		commandMap.put("activateRecruiter", new ActivateCommand());
	}
	
	/**
	 * Private class that contains static field where CommandFactory object is
	 * created. Such schema prevents creation of 2 or more CommandFactory
	 * objects in a multithread environment
	 * (Singleton design pattern)
	 */
	private static class CommandFactoryHolder {
		private static final CommandFactory commandFactory = new CommandFactory();
	}

	/**
	 * Returns the only instance of CommandFactory
	 */
	public static CommandFactory getInstance() {
		return CommandFactoryHolder.commandFactory;
	}

	/**
	 * Returns a proper ICommand class depending on the value of "action"
	 * request parameter.
	 */
	public ICommand getCommand(HttpServletRequest request) {
		ICommand command = null;
		String action = request.getParameter("action");
		command = commandMap.get(action);
		if (command == null) {
			command = new EmptyCommand();
		}
		return command;
	}
}
