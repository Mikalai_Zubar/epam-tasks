package by.mikalai.zubar.dbtask.dao.daofactory;

import java.sql.Connection;

import by.mikalai.zubar.dbtask.dao.AbstractDAO;
import by.mikalai.zubar.dbtask.dao.mysqldao.CandidateDAO;
import by.mikalai.zubar.dbtask.dao.mysqldao.InterviewDAO;
import by.mikalai.zubar.dbtask.dao.mysqldao.RecruterDAO;
import by.mikalai.zubar.dbtask.dao.mysqldao.UserDAO;
import by.mikalai.zubar.dbtask.dao.mysqldao.VacCandConnectorDAO;
import by.mikalai.zubar.dbtask.dao.mysqldao.VacancyDAO;
import by.mikalai.zubar.dbtask.entity.Candidate;
import by.mikalai.zubar.dbtask.entity.Interview;
import by.mikalai.zubar.dbtask.entity.Recruter;
import by.mikalai.zubar.dbtask.entity.User;
import by.mikalai.zubar.dbtask.entity.VacCandConnector;
import by.mikalai.zubar.dbtask.entity.Vacancy;

/**
 * Class  contains methods that create MySQLDAO entities.
 * This is a specific implementation of AbstractDAOFactory that generates
 * classes that allow to work with local MySQL database.
 * @author Mikalay Zubar
 *
 */
public class MySQLDAOFactory extends AbstractDAOFactory{
	
	private Connection connection;
	
	public MySQLDAOFactory(Connection connectin){
		this.connection = connectin;
	}

	@Override
	public AbstractDAO<User> getUserDAO() {
		return new UserDAO(connection);
	}

	@Override
	public AbstractDAO<Candidate> getCandidateDAO() {
		return new CandidateDAO(connection);
	}

	@Override
	public AbstractDAO<Recruter> getRecruterDAO() {
		return new RecruterDAO(connection);
	}

	@Override
	public AbstractDAO<Vacancy> getVacancyDAO() {
		return new VacancyDAO(connection);
	}

	@Override
	public AbstractDAO<Interview> getInterviewDAO() {
		return new InterviewDAO(connection);
	}

	@Override
	public AbstractDAO<VacCandConnector> getVacCandConnectorDAO() {
		return new VacCandConnectorDAO(connection);
	}
	
	
}
