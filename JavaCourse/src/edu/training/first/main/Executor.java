package edu.training.first.main;

import edu.training.first.action.Creator;
import edu.training.first.entity.Point;
import edu.training.first.entity.Triangle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

import static edu.training.first.action.TriangleAction.calcPerimeter;
import static edu.training.first.action.TriangleAction.calcSquare;
import static edu.training.first.action.TriangleAction.calcSquareGeron;

/**
 * Created by Nick on 22.09.16.
 */
public class Executor {

    private static Logger logger = LogManager.getLogger(Creator.class);

    public static void main(String [] args){

        try {
            String[] coordinates = Creator.createPoints();

            Point point1 = new Point(Integer.parseInt(coordinates[0]), Integer.parseInt(coordinates[1]));
            Point point2 = new Point(Integer.parseInt(coordinates[2]), Integer.parseInt(coordinates[3]));
            Point point3 = new Point(Integer.parseInt(coordinates[4]), Integer.parseInt(coordinates[5]));


            Triangle triangle = new Triangle(point1, point2, point3);

            logger.info("Perimeter is: " + calcPerimeter(triangle) );
            logger.info("Square is: " + calcSquare(triangle) );
            logger.info("Geron square is: " + calcSquareGeron(triangle) );
            logger.info("Program finished Ok");
        } catch (IOException e) {
            logger.error("Input filename mistake ", e);
        }catch (NumberFormatException e){
            logger.error("Data input mistake ", e);
        }


    }
}
