package edu.training.first.action;

import java.io.*;

/**
 * Created by Nick on 22.09.16.
 */
public class Creator {

    public static String[] createPoints() throws NumberFormatException, IOException{
        String root = "data/coord.txt";
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(root)));
        String line;
        String coordsLine="";
        while ((line = br.readLine())!=null){
            coordsLine += line+" ";
        }
        String[] coordinates = coordsLine.split(" ");
        return coordinates;

    }
}
