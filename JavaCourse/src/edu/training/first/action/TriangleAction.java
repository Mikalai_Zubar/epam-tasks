package edu.training.first.action;

import edu.training.first.entity.Triangle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by Nick on 22.09.16.
 */
public class TriangleAction {

    private static Logger logger = LogManager.getLogger(TriangleAction.class);


    public static double calcPerimeter(Triangle triangle) {
        double a = Math.pow((triangle.getPoint2().getX() - triangle.getPoint1().getX()), 2)
                + Math.pow((triangle.getPoint2().getY() - triangle.getPoint1().getY()), 2);
        double b = Math.pow((triangle.getPoint3().getX() - triangle.getPoint1().getX()), 2)
                + Math.pow((triangle.getPoint3().getY() - triangle.getPoint1().getY()), 2);
        double c = Math.pow((triangle.getPoint3().getX() - triangle.getPoint2().getX()), 2)
                + Math.pow((triangle.getPoint3().getY() - triangle.getPoint2().getY()), 2);
        if(a+b==c || b+c==a || a+c==b){
            logger.info("The triangle is rectangular");
        }
        double perimeter = Math.sqrt(a) + Math.sqrt(b) + Math.sqrt(c);
        return perimeter;
    }

    public static double calcSquare(Triangle triangle) {
        double e11 = (double)triangle.getPoint1().getX() - triangle.getPoint3().getX();
        double e12 = (double)triangle.getPoint1().getY() - triangle.getPoint3().getY();
        double e21 = (double)triangle.getPoint2().getX() - triangle.getPoint3().getX();
        double e22 = (double)triangle.getPoint2().getY() - triangle.getPoint3().getY();

        double square = Math.abs(e11 * e22 - e21 * e12) / 2;

        if(square==0){
            logger.info("All points belong to one line");
        }
        return square;
    }
    //just to compare to calcSquare, results are the same, inequality should be less than 0,000000001
    public static double calcSquareGeron(Triangle triangle){
        double a = Math.hypot((triangle.getPoint2().getX() - triangle.getPoint1().getX()),
                ((triangle.getPoint2().getY() - triangle.getPoint1().getY())));
        double b = Math.hypot((triangle.getPoint3().getX() - triangle.getPoint1().getX()),
                ((triangle.getPoint3().getY() - triangle.getPoint1().getY())));
        double c = Math.hypot((triangle.getPoint3().getX() - triangle.getPoint2().getX()),
                ((triangle.getPoint3().getY() - triangle.getPoint2().getY())));

        if(a+b==c || a+c==b || b+c==a){
            logger.info("All points belong to one line");
        }

        double p = (a + b + c)/2;

        double square = Math.sqrt(p*(p-a)*(p-b)*(p-c));
        return square;
    }
}
