package edu.training.first.test;

import edu.training.first.entity.Point;
import edu.training.first.entity.Triangle;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static edu.training.first.action.TriangleAction.calcPerimeter;
import static edu.training.first.action.TriangleAction.calcSquare;
import static edu.training.first.action.TriangleAction.calcSquareGeron;

/**
 * Created by Nick on 22.09.16.
 */
public class TriangleTest {
    private Point point1;
    private Point point2;
    private Point point3;
    private Triangle triangle;

    @Before
    public void createPoints() {
        point1 = new Point(3, 6);
        point2 = new Point(9, -2);
        point3 = new Point(-6, -5);
        /* checked results: square == 69, perimeter == 39.509 */
        triangle = new Triangle(point1, point2, point3);
    }

    @Test
    public void calcPerimeterTest(){
        createPoints();
        double expected = 39.509;
        double result = calcPerimeter(triangle);
        Assert.assertEquals("The perimeter is wrong - check calcPerimeter()", expected, result, 0.001);

    }

    @Test
    public void calcSquareTest(){
        createPoints();
        double result = calcSquare(triangle);
        Assert.assertTrue("The square is wrong - check calcSquare()", result==69); //for calcSquare() result always ends .0 or .5

    }

    @Test
    public void calcSquareGeronTest(){
        createPoints();
        double expected = 69;
        double result = calcSquareGeron(triangle);
        Assert.assertEquals("The Geron square is wrong - check calcSquareGeron()", expected, result, 0.001);
    }

}
